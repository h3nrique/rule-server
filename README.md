# Rule Server

Clone
-----
```bash
git clone https://github.com/h3nrique/rule-server.git
```

Compile
-------
```bash
$ cd rule-server
$ mvn clean install -DskipTests
```

Execute
-------
```bash
$ java -jar target/rule-server-1.0.0-SNAPSHOT.jar
```

Test
----
```bash
$ echo $(curl -s -X POST -F 'json={}' http://localhost:9080/rule/process/generic)
$ echo $(curl -s -X POST -F 'json={"json":"valor","idade":"17"}' http://localhost:9080/rule/process/generic)
$ echo $(curl -s -X POST -F 'json={"json":"valor","idade":"18"}' http://localhost:9080/rule/process/generic)
$ echo $(curl -s -X POST -F 'json={"packageName":"br.com.fabricads.poc.model","typeName":"ItemPostal","cep":"60510101"}' http://localhost:9080/rule/process/generic)
$ echo $(curl -s -X POST -F 'json={"packageName":"br.com.fabricads.poc.model","typeName":"ItemPostal","cep":"03694090"}' http://localhost:9080/rule/process/generic)
$ echo $(curl -s -X POST -F 'json={"packageName":"br.com.fabricads.poc.model","typeName":"ItemPostal","cep":"04654010"}' http://localhost:9080/rule/process/generic)
$ echo $(curl -s -X POST -F 'json={"packageName":"br.com.fabricads.poc.model","typeName":"AtualizaInteligencia","codigoPostal": {"packageName": "br.com.fabricads.poc.model", "typeName": "CodigoPostal", "id": 3, "cep1": "04654", "cep2": "010", "logradouro": "Rua Camaratinga", "cidade": "São Paulo", "uf": "SP" }}' http://localhost:9080/rule/process/generic)
$ echo $(curl -s -X POST -F 'json={"packageName":"br.com.fabricads.poc.model","typeName":"ItemPostal","cep":"04654010"}' http://localhost:9080/rule/process/generic)
$ echo $(curl -s -X POST -F 'json={"packageName":"br.com.fabricads.poc.model","typeName":"ObjetoEntrada","ncm":"200","descricao":"CAMISA FARDA UNIFORME","cnae":"100"}' http://localhost:9080/rule/process/generic)
$ echo $(curl -s -X POST -F 'json={"packageName":"br.com.fabricads.poc.model","typeName":"ObjetoEntrada","ncm":"100","descricao":"CAMISA FARDAMENTO UNIFORME","cnae":"100"}' http://localhost:9080/rule/process/generic)
$ echo $(curl -s -X POST -F 'json={"packageName":"br.com.fabricads.poc.model","typeName":"ObjetoEntrada","ncm":"200","descricao":"CAMISA UNIFORME","cnae":"100"}' http://localhost:9080/rule/process/generic)
$ echo $(curl -s -X POST -F 'json={"packageName":"br.com.fabricads.poc.model","typeName":"ObjetoEntrada","ncm":"200","descricao":"TESTE","cnae":"100"}' http://localhost:9080/rule/process/generic)
$ echo $(curl -s -X POST -F 'json={"packageName":"br.com.fabricads.poc.model","typeName":"ObjetoEntrada","ncm":"300","descricao":"TESTE","cnae":"100"}' http://localhost:9080/rule/process/generic)
$ echo $(curl -s -X POST -F 'json={"packageName":"br.com.fabricads.poc.model","typeName":"Transaction","id":"1","value":"10","operation":"DEBIT","entryPoint":"BankStream"}' http://localhost:9080/rule/process/generic)
$ echo $(curl -s -X POST -F 'json={"packageName":"br.com.fabricads.poc.model","typeName":"Transaction","id":"2","value":"10","operation":"DEBIT","entryPoint":"BankStream"}' http://localhost:9080/rule/process/generic)
```
You can also Execute with docker
--------------------------------
```bash
$ sh dockerbuild.sh
$ sh dockerrun.sh
```
