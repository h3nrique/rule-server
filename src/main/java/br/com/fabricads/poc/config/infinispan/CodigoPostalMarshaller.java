package br.com.fabricads.poc.config.infinispan;

import br.com.fabricads.poc.model.CodigoPostal;
import org.infinispan.protostream.MessageMarshaller;

import java.io.IOException;

/**
 * Created by h3nrique on 01/02/17.
 */
public class CodigoPostalMarshaller implements MessageMarshaller<CodigoPostal> {

    @Override
    public Class<? extends CodigoPostal> getJavaClass() {
        return CodigoPostal.class;
    }

    @Override
    public String getTypeName() {
        return CodigoPostal.class.getName();
    }

    @Override
    public CodigoPostal readFrom(ProtoStreamReader protoStreamReader) throws IOException {
        return null;
    }

    @Override
    public void writeTo(ProtoStreamWriter protoStreamWriter, CodigoPostal codigoPostal) throws IOException {

    }
}
