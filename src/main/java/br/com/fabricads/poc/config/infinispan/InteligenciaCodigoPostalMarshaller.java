package br.com.fabricads.poc.config.infinispan;

import br.com.fabricads.poc.model.InteligenciaCodigoPostal;
import org.infinispan.protostream.MessageMarshaller;

import java.io.IOException;

/**
 * Created by h3nrique on 01/02/17.
 */
public class InteligenciaCodigoPostalMarshaller implements MessageMarshaller<InteligenciaCodigoPostal> {

    @Override
    public Class<? extends InteligenciaCodigoPostal> getJavaClass() {
        return InteligenciaCodigoPostal.class;
    }

    @Override
    public String getTypeName() {
        return InteligenciaCodigoPostal.class.getName();
    }

    public InteligenciaCodigoPostal readFrom(MessageMarshaller.ProtoStreamReader var1) throws IOException {
        return null;
    }

    public void writeTo(MessageMarshaller.ProtoStreamWriter var1, InteligenciaCodigoPostal var2) throws IOException {

    }
}
