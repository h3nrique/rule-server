package br.com.fabricads.poc.config.infinispan;

import br.com.fabricads.poc.model.AtualizaInteligencia;
import org.infinispan.protostream.MessageMarshaller;

import java.io.IOException;

/**
 * Created by h3nrique on 01/02/17.
 */
public class AtualizaInteligenciaMarshaller implements MessageMarshaller<AtualizaInteligencia> {

    @Override
    public Class<? extends AtualizaInteligencia> getJavaClass() {
        return AtualizaInteligencia.class;
    }

    @Override
    public String getTypeName() {
        return AtualizaInteligencia.class.getName();
    }

    @Override
    public AtualizaInteligencia readFrom(ProtoStreamReader protoStreamReader) throws IOException {
        return null;
    }

    @Override
    public void writeTo(ProtoStreamWriter protoStreamWriter, AtualizaInteligencia atualizaInteligencia) throws IOException {

    }

}