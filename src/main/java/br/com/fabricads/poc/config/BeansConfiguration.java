package br.com.fabricads.poc.config;

import br.com.fabricads.poc.config.drools.DroolsResource;
import br.com.fabricads.poc.config.drools.DroolsUtil;
import br.com.fabricads.poc.config.drools.KieBuildException;
import br.com.fabricads.poc.config.drools.ResourcePathType;
import br.com.fabricads.poc.config.infinispan.AtualizaInteligenciaMarshaller;
import br.com.fabricads.poc.config.infinispan.CodigoPostalMarshaller;
import br.com.fabricads.poc.config.infinispan.InteligenciaCodigoPostalMarshaller;
import br.com.fabricads.poc.config.infinispan.ItemPostalMarshaller;
import br.com.fabricads.poc.model.AtualizaInteligencia;
import br.com.fabricads.poc.model.CodigoPostal;
import br.com.fabricads.poc.model.InteligenciaCodigoPostal;
import br.com.fabricads.poc.model.ItemPostal;
import lombok.extern.slf4j.Slf4j;
import org.infinispan.client.hotrod.RemoteCache;
import org.infinispan.client.hotrod.RemoteCacheManager;
import org.infinispan.client.hotrod.configuration.ConfigurationBuilder;
import org.infinispan.client.hotrod.marshall.ProtoStreamMarshaller;
import org.infinispan.protostream.SerializationContext;
import org.infinispan.protostream.annotations.ProtoSchemaBuilder;
import org.infinispan.query.remote.client.ProtobufMetadataManagerConstants;
import org.kie.api.runtime.KieSession;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.enterprise.context.ApplicationScoped;
import java.io.*;

/**
 * Created by Paulo Henrique Alves on 21/12/16.
 */
@Slf4j
@Configuration
public class BeansConfiguration {

//	private static final String PROTOBUF_DEFINITION_RESOURCE = "library.proto";

	/**
	 * {@link KieSession} producer.
	 * @return
	 * @throws KieBuildException
	 */
	@Bean
	@ApplicationScoped
	public KieSession kieContainer() throws KieBuildException {
		DroolsResource[] resources = {
			new DroolsResource("rules", ResourcePathType.CLASSPATH),
			// new DroolsResource("Test.drl", ResourcePathType.CLASSPATH),
			// new DroolsResource("decision-table-example.xls", ResourcePathType.CLASSPATH),
			// new DroolsResource("/tmp/Test.drl", ResourcePathType.FILE),
			// new DroolsResource("http://localhost/Test.drl", ResourcePathType.URL)
		};
		return DroolsUtil.createKieSession(resources);
	}

	/**
	 * {@link RemoteCacheManager} producer.
	 * @return
	 * @throws IOException
	 */
	// @Bean
	// @ApplicationScoped
	public RemoteCacheManager remoteCacheManager() throws IOException, KieBuildException {
		ConfigurationBuilder builder = new ConfigurationBuilder()
				.addServers("")
				.marshaller(new ProtoStreamMarshaller()); // The Protobuf based marshaller is required for query capabilities.

		RemoteCacheManager remoteCacheManager = new RemoteCacheManager(builder.build());
		try {
			registerSchemasAndMarshallers(remoteCacheManager);
		} catch (Exception err) {
			if(log.isDebugEnabled()) {
				log.error("Error while register schemas and marshallers.", err);
			} else {
				log.error("Error while register schemas and marshallers.");
			}
		}

		return remoteCacheManager;
	}

	/**
	 * Register Schemas ans Marshaller.
	 * @param remoteCacheManager
	 * @throws IOException
	 */
	private void registerSchemasAndMarshallers(RemoteCacheManager remoteCacheManager) throws IOException {
		// Register entity marshallers on the client side ProtoStreamMarshaller instance associated with the remote cache manager.
		SerializationContext ctx = ProtoStreamMarshaller.getSerializationContext(remoteCacheManager);
//		ctx.registerProtoFiles(FileDescriptorSource.fromResources(PROTOBUF_DEFINITION_RESOURCE));

		// generate the 'memo.proto' schema file based on the annotations on Memo class and register it with the SerializationContext of the client
		ProtoSchemaBuilder protoSchemaBuilder = new ProtoSchemaBuilder();
		String codigoPostalSchemaFile = protoSchemaBuilder
				.fileName(CodigoPostal.class.getSimpleName().toLowerCase().concat(".proto"))
				.packageName(CodigoPostal.class.getPackage().getName())
				.addClass(CodigoPostal.class)
				.build(ctx);
		String atualizaInteligenciaSchemaFile = protoSchemaBuilder
				.fileName(AtualizaInteligencia.class.getSimpleName().toLowerCase().concat(".proto"))
				.packageName(AtualizaInteligencia.class.getPackage().getName())
				.addClass(AtualizaInteligencia.class)
				.build(ctx);
		String inteligenciaCodigoPostalSchemaFile = protoSchemaBuilder
				.fileName(InteligenciaCodigoPostal.class.getSimpleName().toLowerCase().concat(".proto"))
				.packageName(InteligenciaCodigoPostal.class.getPackage().getName())
				.addClass(InteligenciaCodigoPostal.class)
				.build(ctx);
		String itemPostalSchemaFile = protoSchemaBuilder
				.fileName(ItemPostal.class.getSimpleName().toLowerCase().concat(".proto"))
				.packageName(ItemPostal.class.getPackage().getName())
				.addClass(ItemPostal.class)
				.build(ctx);

		ctx.registerMarshaller(new InteligenciaCodigoPostalMarshaller());
		ctx.registerMarshaller(new CodigoPostalMarshaller());
		ctx.registerMarshaller(new ItemPostalMarshaller());
		ctx.registerMarshaller(new AtualizaInteligenciaMarshaller());

		// register the schemas with the server too
		RemoteCache<String, String> metadataCache = remoteCacheManager.getCache(ProtobufMetadataManagerConstants.PROTOBUF_METADATA_CACHE_NAME);
//		metadataCache.put(PROTOBUF_DEFINITION_RESOURCE, readResource(PROTOBUF_DEFINITION_RESOURCE));
		metadataCache.put(CodigoPostal.class.getSimpleName().toLowerCase().concat(".proto"), codigoPostalSchemaFile);
		metadataCache.put(AtualizaInteligencia.class.getSimpleName().toLowerCase().concat(".proto"), atualizaInteligenciaSchemaFile);
		metadataCache.put(InteligenciaCodigoPostal.class.getSimpleName().toLowerCase().concat(".proto"), inteligenciaCodigoPostalSchemaFile);
		metadataCache.put(ItemPostal.class.getSimpleName().toLowerCase().concat(".proto"), itemPostalSchemaFile);
		String errors = metadataCache.get(ProtobufMetadataManagerConstants.ERRORS_KEY_SUFFIX);
		if (errors != null) {
			throw new IllegalStateException("Some Protobuf schema files contain errors:\n" + errors);
		}
	}

	/**
	 * Read resource input stream.
	 * @param resourcePath
	 * @return
	 * @throws IOException
	 */
	private String readResource(String resourcePath) throws IOException {
		InputStream is = getClass().getResourceAsStream(resourcePath);
		try {
			final Reader reader = new InputStreamReader(is, "UTF-8");
			StringWriter writer = new StringWriter();
			char[] buf = new char[1024];
			int len;
			while ((len = reader.read(buf)) != -1) {
				writer.write(buf, 0, len);
			}
			return writer.toString();
		} finally {
			is.close();
		}
	}
}