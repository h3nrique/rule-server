package br.com.fabricads.poc.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Transaction implements Serializable {
    private String id;
    private Integer value;
    private Operation operation;
    private Date timestamp;
    private boolean denied;

}
