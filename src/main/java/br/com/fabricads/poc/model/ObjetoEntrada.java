package br.com.fabricads.poc.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Created by h3nrique on 16/03/17.
 */
@AllArgsConstructor
@NoArgsConstructor
public @Data class ObjetoEntrada implements Serializable {

    public Integer ncm;
    public String descricao;
    public Integer cnae;
    public String resultado;
}