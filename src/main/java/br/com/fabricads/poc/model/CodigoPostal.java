package br.com.fabricads.poc.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.infinispan.protostream.annotations.ProtoDoc;
import org.infinispan.protostream.annotations.ProtoField;

import java.io.Serializable;

/**
 * Created by h3nrique on 01/02/17.
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ProtoDoc("@Indexed")
public class CodigoPostal implements Serializable {

    @Setter
    private Long id;
    @Setter
    @ProtoDoc("@IndexedField")
    private String cep1;
    @Setter
    private String cep2;
    @Setter
    @ProtoDoc("@IndexedField")
    private String logradouro;
    @Setter
    @ProtoDoc("@IndexedField")
    private String cidade;
    @Setter
    @ProtoDoc("@IndexedField")
    private String uf;

    @ProtoField(number = 1, required = true)
    public Long getId() {
        return id;
    }

    @ProtoField(number = 2, required = true)
    public String getCep1() {
        return cep1;
    }

    @ProtoField(number = 3, required = true)
    public String getCep2() {
        return cep2;
    }

    @ProtoField(number = 4, required = true)
    public String getLogradouro() {
        return logradouro;
    }

    @ProtoField(number = 5, required = true)
    public String getCidade() {
        return cidade;
    }

    @ProtoField(number = 6, required = true)
    public String getUf() {
        return uf;
    }
}