package br.com.fabricads.poc;

import br.com.fabricads.poc.services.RuleService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

/**
 * Created by Paulo Henrique Alves on 21/12/16.
 */
@SpringBootApplication
@ComponentScan
@EnableAutoConfiguration
public class RuleServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(RuleServerApplication.class, args);
    }

    @Bean
    CommandLineRunner init(final RuleService ruleService, @Value("${br.com.fabricads.mock}") Boolean mock) {
        return arg -> {
            // Mock inteligencia
            if(mock) {
                String cepInteligencia1 = "{\"packageName\":\"br.com.fabricads.poc.model\",\"typeName\":\"AtualizaInteligencia\",\"codigoPostal\": {\"packageName\": \"br.com.fabricads.poc.model\", \"typeName\": \"CodigoPostal\", \"id\": 1, \"cep1\": \"03694\", \"cep2\": \"090\", \"logradouro\": \"Rua Deodato Saraiva da Silva\", \"cidade\": \"São Paulo\", \"uf\": \"SP\" }}";
                String cepInteligencia2 = "{\"packageName\":\"br.com.fabricads.poc.model\",\"typeName\":\"AtualizaInteligencia\",\"codigoPostal\": {\"packageName\": \"br.com.fabricads.poc.model\", \"typeName\": \"CodigoPostal\", \"id\": 2, \"cep1\": \"60510\", \"cep2\": \"101\", \"logradouro\": \"Rua George Rocha\", \"cidade\": \"Fortaleza\", \"uf\": \"CE\" }}";
                String cepInteligencia3 = "{\"packageName\":\"br.com.fabricads.poc.model\",\"typeName\":\"AtualizaInteligencia\",\"codigoPostal\": {\"packageName\": \"br.com.fabricads.poc.model\", \"typeName\": \"CodigoPostal\", \"id\": 3, \"cep1\": \"04654\", \"cep2\": \"010\", \"logradouro\": \"Rua Samuel Endler\", \"cidade\": \"São Paulo\", \"uf\": \"SP\" }}";
                ruleService.fireFules(cepInteligencia1);
                ruleService.fireFules(cepInteligencia2);
                ruleService.fireFules(cepInteligencia3);
            }
        };
    }
}