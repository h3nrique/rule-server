package br.com.fabricads.poc.services;

/**
 * Created by Paulo Henrique Alves on 21/12/16.
 */
public interface RuleService {
    String IDADE_REFERENCIA = "IDADE_REFERENCIA";

    String fireFules(String json);
}